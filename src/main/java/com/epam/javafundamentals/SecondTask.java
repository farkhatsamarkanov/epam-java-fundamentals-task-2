package com.epam.javafundamentals;

public class SecondTask
{
    public static void main( String[] args )
    {
        InputHandler inputHandler = new InputHandler();   //instantiating input handling class
        inputHandler.processIntegersFromInput();
    }
}
