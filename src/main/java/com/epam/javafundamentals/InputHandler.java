package com.epam.javafundamentals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class InputHandler {
    public void processIntegersFromInput() {
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter a sequence of integers separated by spacings...");
        boolean repeatInputRequest = true;                                                 //implementing continuous app execution
        while (repeatInputRequest) {
            try {
                String typedIntegers = inputReader.readLine();                              //reading input line
                String[] arrayOfSeparatedIntegers = typedIntegers.split(" ");         //splitting input line to separate strings
                if (!typedIntegers.equals("")) {                                            //checking if input is empty
                    ArrayList<Integer> filterArrayList = new ArrayList<>();                 //temporary ArrayList to check if input data are integers
                    for (int i = 0; i < arrayOfSeparatedIntegers.length; i++) {            //looping through entered data
                        boolean correctInput = true;
                        try {
                            Integer.valueOf(arrayOfSeparatedIntegers[i]);                  //checking if entered data is integer
                        } catch (NumberFormatException e) {
                            correctInput = false;
                            System.out.println("Element at index " + i + " is not an integer!");
                        }
                        if (correctInput) {
                            filterArrayList.add(Integer.valueOf(arrayOfSeparatedIntegers[i]));
                        }
                    }
                    if (filterArrayList.size() != 0) {                                     //checking if any of entered data are integers
                        repeatInputRequest = false;
                        int[] parsedIntegers = new int[filterArrayList.size()];
                        for (int i = 0; i < filterArrayList.size(); i++) {
                            parsedIntegers[i] = filterArrayList.get(i);
                        }
                        System.out.println();
                        findShortestAndLongestInteger(parsedIntegers);                     //executing entered integers processing methods
                        findIntegerWithLeastDifferentDigits(parsedIntegers);
                        findIntegerWithAscendingDigits(parsedIntegers);
                        findIntegerWithUniqueDigitsOnly(parsedIntegers);
                    } else {
                        System.out.println("\nAll of entered data are not integers! Please try again...");
                    }
                } else {
                    System.out.println("Entered sequence is empty! Please try again...");
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        try {
            inputReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void findShortestAndLongestInteger(int[] arrayOfIntegers) {                   //method to find shortest and longest integer in input line
        int[] lengthsOfIntegers = new int[arrayOfIntegers.length];                        //temporary array with integers' lengths
        for (int i = 0; i < lengthsOfIntegers.length; i++) {
            lengthsOfIntegers[i] = countDigitsInInteger(arrayOfIntegers[i]);
        }
        int maxIntegerLength;
        int minIntegerLength;
        maxIntegerLength = minIntegerLength = lengthsOfIntegers[0];
        int maxIntegerLengthIndex;
        int minIntegerLengthIndex;
        maxIntegerLengthIndex = minIntegerLengthIndex = 0;
        for (int i = 0; i < lengthsOfIntegers.length; i++) {                              //for loop to find shortest and longest integer
            if (lengthsOfIntegers[i] > maxIntegerLength) {
                maxIntegerLength = lengthsOfIntegers[i];
                maxIntegerLengthIndex = i;
            } else if (lengthsOfIntegers[i] < minIntegerLength) {
                minIntegerLength = lengthsOfIntegers[i];
                minIntegerLengthIndex = i;
            }
        }
        System.out.println("First shortest integer in the sequence: " + arrayOfIntegers[minIntegerLengthIndex] + ", length: " + minIntegerLength);
        System.out.println("First longest integer in the sequence: " + arrayOfIntegers[maxIntegerLengthIndex] + ", length: " + maxIntegerLength);
    }

    private int countDigitsInInteger(int integer) {                                      //method to count digits in integer
        int digitCounter = integer == 0 ? 1 : 0;
        while (integer != 0) {
            integer /= 10;
            ++digitCounter;
        }
        return digitCounter;
    }

    private int[] getDigitsOfInteger(int integer) {                                     //method to get each digit of an integer
        int[] digitsOfInteger = new int[countDigitsInInteger(integer)];
        for (int i = digitsOfInteger.length - 1; i >= 0; i--) {
            digitsOfInteger[i] = integer % 10;
            integer /= 10;
        }
        return digitsOfInteger;
    }

    private static int countUniqueDigits(int[] arrayOfDigits) {                        //method to count unique digits in an array of digits
        HashSet<Integer> set = new HashSet<>();
        int uniqueDigitsCounter = 0;
        for (int setElement : arrayOfDigits) {
            if (set.add(setElement)) {
                uniqueDigitsCounter += 1;
            }
        }
        return uniqueDigitsCounter;
    }

    private void findIntegerWithLeastDifferentDigits(int[] arrayOfIntegers) {          //method to find an integer with least different digits
        int[] uniqueDigitsCountersArray = new int[arrayOfIntegers.length];
        for (int i = 0; i < arrayOfIntegers.length; i++)                               //loop through integers and count unique digits in them
        {
            uniqueDigitsCountersArray[i] = countUniqueDigits(getDigitsOfInteger(arrayOfIntegers[i]));
        }
        int minValueOfUniqueDigits = uniqueDigitsCountersArray[0];
        int minValueOfUniqueDigitsIndex = 0;
        for (int i = 1; i < uniqueDigitsCountersArray.length; i++)                     //looping through an array of unique digits counts to find min value
        {
            if (uniqueDigitsCountersArray[i] < minValueOfUniqueDigits) {
                minValueOfUniqueDigitsIndex = i;
                minValueOfUniqueDigits = uniqueDigitsCountersArray[i];
            }
        }
        System.out.println("First integer with least different digits: " + arrayOfIntegers[minValueOfUniqueDigitsIndex]);
    }

    private void findIntegerWithAscendingDigits(int[] arrayOfIntegers) {              //method to find the first integer with ascending digits
        boolean necessaryIntegerFound = false;
        for (int integer : arrayOfIntegers) {                                          //loop through an array of integers
            if (countDigitsInInteger(integer) > 1) {                                   //checking if an integer has only 1 digit
                int[] digitsOfInteger = getDigitsOfInteger(integer);                   //getting digits of an integer
                boolean ascendingDigitsInteger = true;
                for (int j = 0; j < digitsOfInteger.length - 1; j++) {                 //looping through digits of an integer
                    if (digitsOfInteger[j] > digitsOfInteger[j + 1]) {                 //comparing sequential digits to check ascending order
                        ascendingDigitsInteger = false;
                        break;
                    }
                }
                if (ascendingDigitsInteger) {
                    necessaryIntegerFound = true;
                    System.out.println("First integer with digits in ascending order: " + integer);
                    break;
                }
            }
        }
        if (!necessaryIntegerFound) {
            System.out.println("No integer with digits in ascending order");
        }
    }

    private void findIntegerWithUniqueDigitsOnly(int[] arrayOfIntegers) {                                 //method to find an integer with only unique digits
        boolean necessaryIntegerFound = false;
        for (int integer : arrayOfIntegers) {                                                              //loop through an array of integers
            if (countDigitsInInteger(integer) > 1) {                                                       //checking if an integer has only 1 digit
                if (countDigitsInInteger(integer) == countUniqueDigits(getDigitsOfInteger(integer))) {     //checking if number of unique digits equals to number of digits in an integer
                    necessaryIntegerFound = true;
                    System.out.println("First integer with unique digits only " + integer);
                    break;
                }
            }
        }
        if (!necessaryIntegerFound) {
            System.out.println("No integer with unique digits only");
        }
    }

}
